const suppliersDb = ({ makeDb }) => {
  return Object.freeze({
    insert,
    findById,
    findAll,
    update,
    remove,
    findByAll
  });
  async function insert({ ...suppliersInfo }) {
    const db = await makeDb();
    const sql = "INSERT INTO suppliers VALUES (DEFAULT,$1,$2);";
    const params = [suppliersInfo.name, suppliersInfo.address];
    return db.query(sql, params);
  }
  //
  async function findById(id) {
    const db = await makeDb();
    const sql = "SELECT * FROM suppliers WHERE id=$1";
    const params = [id];
    return db.query(sql, params);
  }
  //
  async function findAll() {
    const db = await makeDb();
    const sql = "SELECT * FROM suppliers";
    return db.query(sql);
  }
  //
  async function update({ ...suppliersInfo }) {
    const db = await makeDb();
    const sql = "UPDATE suppliers SET name=$1, address=$2 WHERE id=$3";
    const params = [
      suppliersInfo.name,
      suppliersInfo.address,
      suppliersInfo.id
    ];
    return db.query(sql, params);
  }
  //
  async function remove({ id }) {
    const db = await makeDb();
    const sql = "DELETE FROM suppliers WHERE id=$1;";
    const params = [id];
    return db.query(sql, params);
  }
  //
  async function findByAll(name, address) {
    console.log("name? ", name);
    const db = await makeDb();
    const sql = "SELECT * FROM suppliers WHERE name=$1 AND address=$2";
    const params = [name, address];
    return db.query(sql, params);
  }
};

module.exports = suppliersDb;
