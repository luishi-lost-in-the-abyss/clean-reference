const employeesDb = require("./employees-db");

const { Client } = require("pg");
const dotenv = require("dotenv");

dotenv.config();

const client = new Client();

// function to connect to db
async function makeDb() {
  if (!client.connect()) {
    await client.connect();
  }
  return client;
}

const employeeDb = employeesDb({ makeDb });

module.exports = employeeDb;
