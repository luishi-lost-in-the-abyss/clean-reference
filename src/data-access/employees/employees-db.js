const employeesDb = ({ makeDb }) => {
  return Object.freeze({
    insert,
    findById,
    findAll,
    update,
    remove
  });
  async function insert({ ...employeeInfo }) {
    const db = await makeDb();
    const sql = "INSERT INTO employees VALUES ($1,$2,$3,$4,$5,$6);";
    const params = [
      employeeInfo.id,
      employeeInfo.firstname,
      employeeInfo.lastname,
      employeeInfo.middlename,
      employeeInfo.images,
      employeeInfo.email
    ];
    return db.query(sql, params);
  }
  //
  async function findById(id) {
    const db = await makeDb();
    const sql = "SELECT * FROM employees WHERE id=$1";
    const params = [id];
    return db.query(sql, params);
  }
  //
  async function findAll() {
    const db = await makeDb();
    const sql = "SELECT * FROM employees";
    return db.query(sql);
  }
  //
  async function update({ ...employeeInfo }) {
    const db = await makeDb();
    const image = employeeInfo.images;
    if (!image) {
      const sql =
        "UPDATE employees SET firstname=$1, lastname=$2, middlename=$3, email=$4 WHERE id=$5";
      const params = [
        employeeInfo.firstname,
        employeeInfo.lastname,
        employeeInfo.middlename,
        employeeInfo.email,
        employeeInfo.id
      ];
      return db.query(sql, params);
    } else {
      const sql =
        "UPDATE employees SET firstname=$1, lastname=$2, middlename=$3, image=$4, email=$5 WHERE id=$6";
      const params = [
        employeeInfo.firstname,
        employeeInfo.lastname,
        employeeInfo.middlename,
        employeeInfo.images,
        employeeInfo.email,
        employeeInfo.id
      ];
      return db.query(sql, params);
    }
  }
  //
  async function remove({ id }) {
    const db = await makeDb();
    const sql = "DELETE FROM employees WHERE id=$1;";
    const params = [id];
    return db.query(sql, params);
  }
};

module.exports = employeesDb;
