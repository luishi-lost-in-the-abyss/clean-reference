const express = require("express");
const dotenv = require("dotenv");
const {
  insertEmployees,
  selectEmployees,
  selectOneEmployees,
  updateEmployees,
  deleteEmployee
} = require("./controllers/employees/app");

const {
  insertSupplier,
  selectAllSupplier,
  selectOneSupplier,
  updateSupplier,
  removeSupplier
} = require("./controllers/suppliers/app");

const makeExpressCallback = require("./express-callback/app");
dotenv.config();

const app = express();
app.use(express.json());
app.use((_, res, next) => {
  res.set({ Tk: "!" });
  next();
});
// employees
app.post("/employee/add", makeExpressCallback(insertEmployees));
app.get("/employee", makeExpressCallback(selectEmployees));
app.get("/employee/:id", makeExpressCallback(selectOneEmployees));
app.put("/employee/update/:id", makeExpressCallback(updateEmployees));
app.delete("/employee/delete/:id", makeExpressCallback(deleteEmployee));
// end
// suppliers
app.post("/suppliers/add", makeExpressCallback(insertSupplier));
app.get("/suppliers", makeExpressCallback(selectAllSupplier));
app.get("/suppliers/:id", makeExpressCallback(selectOneSupplier));
app.put("/suppliers/update/:id", makeExpressCallback(updateSupplier));
app.delete("/suppliers/delete/:id", makeExpressCallback(removeSupplier));
// end
const PORT = process.env.PORT || 5000;

if (process.env.DM_ENV === "dev") {
  // listen for requests
  app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}.`);
  });
}

module.exports = app;
