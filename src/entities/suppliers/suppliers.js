const makeSupplierEntity = ({}) => {
  return function makeSupplier({ name, address } = {}) {
    if (!name) {
      throw new Error("Please check company name input data..");
    }
    if (!address) {
      throw new Error("Please check company address input data..");
    }

    return Object.freeze({
      getName: () => name,
      getAddress: () => address
    });
  };
};

module.exports = makeSupplierEntity;
