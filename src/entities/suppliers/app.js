const makeSupplierEntity = require("./suppliers");

const makeSupplier = makeSupplierEntity({});

const supplierEntity = Object.freeze({
  makeSupplier
});

module.exports = supplierEntity;

module.exports = {
  makeSupplier
};
