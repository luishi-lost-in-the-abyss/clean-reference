const makeEmployeeEntityUpdate = ({ validateEmail }) => {
  return function makeEmployee({
    firstname,
    lastname,
    middlename,
    images,
    email
  } = {}) {
    if (!firstname || hasNumber(firstname)) {
      throw new Error("Please check first name input data..");
    }
    if (!lastname || hasNumber(lastname)) {
      throw new Error("Please check last name input data..");
    }
    if (!middlename || hasNumber(middlename)) {
      throw new Error("Please check middle name input data..");
    }
    if (validateEmail(email) === false) {
      throw new Error("Invalid email format..");
    }

    return Object.freeze({
      getFirstName: () => firstname,
      getLastName: () => lastname,
      getMiddleName: () => middlename,
      getImages: () => images,
      getEmail: () => email
    });
  };
};

function hasNumber(myString) {
  return /\d/.test(myString);
}

module.exports = makeEmployeeEntityUpdate;
