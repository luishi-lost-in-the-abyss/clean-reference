const isemail = require("isemail");
const makeEmployeeEntity = require("./employees");
const makeEmployeeEntityUpdate = require("./employees-update-db");

const makeEmployee = makeEmployeeEntity({ validateEmail });
const makeEmployeeUpdate = makeEmployeeEntityUpdate({ validateEmail });

function validateEmail(email) {
  return isemail.validate(email);
}

const employeeEntity = Object.freeze({
  makeEmployee,
  makeEmployeeUpdate
});

module.exports = employeeEntity;

module.exports = {
  makeEmployee,
  makeEmployeeUpdate
};
