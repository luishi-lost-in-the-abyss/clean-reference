const { makeSupplier } = require("../../entities/suppliers/app");

const makeSuppliers = ({ suppliersDb }) => {
  return async function useCaseSupplierInsert(suppliersInfo) {
    const info = makeSupplier(suppliersInfo);
    const exists = await suppliersDb.findByAll(
      info.getName(),
      info.getAddress()
    );
    if (exists.rowCount !== 0) {
      return exists;
    }

    return suppliersDb.insert({
      name: info.getName(),
      address: info.getAddress()
    });
  };
};

module.exports = makeSuppliers;
