const { makeSupplier } = require("../../entities/suppliers/app");

const updateSupplier = ({ suppliersDb }) => {
  return async function putSupplier({ id, ...suppliersInfo } = {}) {
    const suppliers = makeSupplier(suppliersInfo);
    //
    const updated = await suppliersDb.update({
      id: id,
      name: suppliers.getName(),
      address: suppliers.getAddress()
    });
    return { ...updated };
  };
};

module.exports = updateSupplier;
