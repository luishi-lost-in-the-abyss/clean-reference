const suppliersDb = require("../../data-access/suppliers/app");

const makeSuppliers = require("./suppliers-add");
const useCaseSelectAllSuppliers = require("./suppliers-selectAll");
const selectOneSupplier = require("./suppliers-selectOne");
const updateSupplier = require("./suppliers-update");
const removeSupplier = require("./suppliers-delete");

const insertSuppliers = makeSuppliers({ suppliersDb });
const selectAllSuppliers = useCaseSelectAllSuppliers({ suppliersDb });
const selectOneSuppliers = selectOneSupplier({ suppliersDb });
const updateSuppliers = updateSupplier({ suppliersDb });
const removeSuppliers = removeSupplier({ suppliersDb });

const supplierService = Object.freeze({
  insertSuppliers,
  selectAllSuppliers,
  selectOneSuppliers,
  updateSuppliers,
  removeSuppliers
});

module.exports = supplierService;

module.exports = {
  insertSuppliers,
  selectAllSuppliers,
  selectOneSuppliers,
  updateSuppliers,
  removeSuppliers
};
