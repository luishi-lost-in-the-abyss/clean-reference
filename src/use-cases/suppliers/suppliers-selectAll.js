const useCaseSelectAllSuppliers = ({ suppliersDb }) => {
    return async function selectSuppliers() {
      const suppliers = await suppliersDb.findAll();
      return suppliers.rows;
    };
  };
  
  module.exports = useCaseSelectAllSuppliers;
  