const selectOneSupplier = ({ suppliersDb }) => {
  return async function selectOneSuppliers(id) {
    const supplier = await suppliersDb.findById(id);
    return supplier.rows;
  };
};

module.exports = selectOneSupplier;
