const removeSupplier = ({ suppliersDb }) => {
    return async function deleteSupplier({ id } = {}) {
      const supplierToRemove = await suppliersDb.findById(id);
      if (supplierToRemove.rowCount === 0) {
        return deleteNothing();
      }
      //
      const deleted = await suppliersDb.remove({
        id
      });
      return {
        deletedCount: 1,
        msg: "Supplier is found, deleted...",
        supplierToRemove,
        deleted
      };
  
      function deleteNothing() {
        return {
          deletedCount: 0,
          msg: "Supplier not found, nothing to delete."
        };
      }
    };
  };
  
  module.exports = removeSupplier;
  