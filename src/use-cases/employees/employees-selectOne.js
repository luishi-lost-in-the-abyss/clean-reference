const selectOneEmployee = ({ employeeDb }) => {
  return async function selectOneEmployees(id) {
    const employees = await employeeDb.findById(id);
     return employees.rows;
  };
};

module.exports = selectOneEmployee;
