const { makeEmployeeUpdate } = require("../../entities/employees/app");

const updateEmployees = ({ employeeDb }) => {
  return async function putEmployee({ id, ...employeeInfo } = {}) {
    const employee = makeEmployeeUpdate(employeeInfo);
    //
    const updated = await employeeDb.update({
      id: id,
      firstname: employee.getFirstName(),
      lastname: employee.getLastName(),
      middlename: employee.getMiddleName(),
      images: employee.getImages(),
      email: employee.getEmail()
    });
    return { ...updated };
  };
};

module.exports = updateEmployees;
