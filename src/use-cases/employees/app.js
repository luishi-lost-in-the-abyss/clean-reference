const employeeDb = require("../../data-access/employees/app");

const makeEmployees = require("./employees-insert");
const selectAllEmployees = require("./employees-selectAll");
const selectOneEmployees = require("./employees-selectOne");
const updateEmployees = require("./employees-update");
const removeEmployees = require("./employees-delete");

const insertEmployee = makeEmployees({ employeeDb });
const selectAllEmployee = selectAllEmployees({ employeeDb });
const selectOneEmployee = selectOneEmployees({ employeeDb });
const updateEmployee = updateEmployees({ employeeDb });
const removeEmployee = removeEmployees({ employeeDb });

const employeeService = Object.freeze({
  insertEmployee,
  selectAllEmployee,
  selectOneEmployee,
  updateEmployee,
  removeEmployee
});

module.exports = employeeService;

module.exports = {
  insertEmployee,
  selectAllEmployee,
  selectOneEmployee,
  updateEmployee,
  removeEmployee
};
