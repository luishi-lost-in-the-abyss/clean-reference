const removeEmployees = ({ employeeDb }) => {
  return async function deleteEmployee({ id } = {}) {
    const employeeToRemove = await employeeDb.findById(id);
    if (employeeToRemove.rowCount === 0) {
      return deleteNothing();
    }
    //
    const deleted = await employeeDb.remove({
      id
    });
    return {
      deletedCount: 1,
      msg: "Employee is found, deleted...",
      employeeToRemove,
      deleted
    };

    function deleteNothing() {
      return {
        deletedCount: 0,
        msg: "Employee not found, nothing to delete."
      };
    }
  };
};

module.exports = removeEmployees;
