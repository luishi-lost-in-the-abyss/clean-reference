const useCaseSelectAllEmployees = ({ employeeDb }) => {
  return async function selectEmployee() {
    const employee = await employeeDb.findAll();
    return employee.rows;
  };
};

module.exports = useCaseSelectAllEmployees;
