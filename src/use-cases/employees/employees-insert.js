const { makeEmployee } = require("../../entities/employees/app");

const makeEmployees = ({ employeeDb }) => {
  return async function useCaseEmployeeInsert(employeeInfo) {
    const info = makeEmployee(employeeInfo);
    const exists = await employeeDb.findById(info.getId());
    if (exists.rowCount !== 0) {
      return exists;
    }

    return employeeDb.insert({
      id: info.getId(),
      firstname: info.getFirstName(),
      middlename: info.getMiddleName(),
      lastname: info.getLastName(),
      images: info.getImages(),
      email: info.getEmail()
    });
  };
};

module.exports = makeEmployees;
