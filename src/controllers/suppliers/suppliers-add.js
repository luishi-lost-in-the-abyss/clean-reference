const controllerMakeSupplier = ({ insertSuppliers }) => {
  return async function postSupplier(httpRequest) {
    try {
      const { source = {}, ...supplierInfo } = httpRequest.body;
      source.ip = httpRequest.ip;
      source.browser = httpRequest.headers["User-Agent"];
      if (httpRequest.headers["Referer"]) {
        source.referrer = httpRequest.headers["Referer"];
      }
      console.log("supplierInfo? ", supplierInfo);
      const posted = await insertSuppliers({
        ...supplierInfo,
        source
      });
      return {
        headers: {
          "Content-Type": "application/json",
          "Last-Modified": new Date(posted.modifiedOn).toUTCString()
        },
        statusCode: 201,
        body: { posted }
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers: {
          "Content-Type": "application/json"
        },
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = controllerMakeSupplier;
