const controllerMakeSupplier = require("./suppliers-add");
const controllerSelectAllSuppliers = require("./suppliers-selectAll");
const controllerSelectOneSupplier = require("./suppliers-selectOne");
const controllerUpdateSupplier = require("./suppliers-update");
const controllerRemoveSupplier = require("./suppliers-delete");

const {
  insertSuppliers,
  selectAllSuppliers,
  selectOneSuppliers,
  updateSuppliers,
  removeSuppliers
} = require("../../use-cases/suppliers/app");

const insertSupplier = controllerMakeSupplier({ insertSuppliers });
const selectAllSupplier = controllerSelectAllSuppliers({ selectAllSuppliers });
const selectOneSupplier = controllerSelectOneSupplier({ selectOneSuppliers });
const updateSupplier = controllerUpdateSupplier({ updateSuppliers });
const removeSupplier = controllerRemoveSupplier({ removeSuppliers });

const supplierController = Object.freeze({
  insertSupplier,
  selectAllSupplier,
  selectOneSupplier,
  updateSupplier,
  removeSupplier
});

//---------------------------------->
module.exports = supplierController;
module.exports = {
  insertSupplier,
  selectAllSupplier,
  selectOneSupplier,
  updateSupplier,
  removeSupplier
};
