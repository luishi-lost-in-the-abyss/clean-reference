const controllerSelectAllSuppliers = ({ selectAllSuppliers }) => {
    // selectAnime must have the same name all through out
    return async function selectEmployees(httpRequest) {
      const headers = {
        "Content-Type": "application/json"
      };
      try {
        const postSuppliers = await selectAllSuppliers({
          postId: httpRequest.query.postId
        });
        return {
          headers,
          statusCode: 200,
          body: postSuppliers
        };
      } catch (e) {
        // TODO: Error logging
        console.log(e);
        return {
          headers,
          statusCode: 400,
          body: {
            error: e.message
          }
        };
      }
    };
  };
  
  module.exports = controllerSelectAllSuppliers;
  