const controllerMakeEmployee = require("./employees-insert");
const controllerSelectAllEmployees = require("./employees-selectAll");
const controllerSelectOneEmployee = require("./employees-selectOne");
const controllerUpdateEmployee = require("./employees-update");
const controllerEmployeeRemove = require("./employees-delete");

const {
  insertEmployee,
  selectAllEmployee,
  selectOneEmployee,
  updateEmployee,
  removeEmployee
} = require("../../use-cases/employees/app");

const insertEmployees = controllerMakeEmployee({ insertEmployee });
const selectEmployees = controllerSelectAllEmployees({ selectAllEmployee });
const selectOneEmployees = controllerSelectOneEmployee({ selectOneEmployee });
const updateEmployees = controllerUpdateEmployee({ updateEmployee });
const deleteEmployee = controllerEmployeeRemove({ removeEmployee });

const employeeController = Object.freeze({
  insertEmployee,
  selectEmployees,
  selectOneEmployees,
  updateEmployees,
  deleteEmployee
});

//---------------------------------->
module.exports = employeeController;
module.exports = {
  insertEmployees,
  selectEmployees,
  selectOneEmployees,
  updateEmployees,
  deleteEmployee
};
