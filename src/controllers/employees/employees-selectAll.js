const controllerSelectAllEmployees = ({ selectAllEmployee }) => {
  // selectAnime must have the same name all through out
  return async function selectEmployees(httpRequest) {
    const headers = {
      "Content-Type": "application/json"
    };
    try {
      const postEmployees = await selectAllEmployee({
        postId: httpRequest.query.postId
      });
      return {
        headers,
        statusCode: 200,
        body: postEmployees
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);
      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message
        }
      };
    }
  };
};

module.exports = controllerSelectAllEmployees;
